#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h> // for open
#include <pthread.h> // for threads

void execute(char **array, int arraylen, int error, int parallel);

struct threadInfo{
	char **array;
	int  arraylen;
	int  error;
	int   parallel;
};

struct threadInfo info;

void *executable(void *args){
	//struct threadInfo *info = args;
	execute(info.array, info.arraylen, info.error, info.parallel);
	pthread_exit(NULL);
    return NULL;
}

void printerror(){
	char error_message[30] = "An error has occurred\n";
    write(STDERR_FILENO, error_message, strlen(error_message)); 
}

char *paths[10] = {NULL, "/bin/", "/usr/bin/"}; // the null is for the command the user enters, assuming that the user may enter the entire path of the command. For example, /bin/ls. It should still work regardless

// this function takes the users command that has been splitted by spaces into an array and the various paths the program can access
//if error is 1, it means there are Multiple redirection operators or multiple files to the right of the redirection sign
//if parallel is 1, it means the user wants to execute parallel commands
void execute(char **array, int arraylen, int error, int parallel){ 
	char *ex = "exit";
	char *pa = "path";
	char *cd = "cd";
	//printf("%d %d %d %s\n", arraylen, error, parallel, array[0]);

	if(parallel == 1){
		char *newarray[10] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};
		int a = 0;
		int merror = -1;
		int start = 0;
		while(a < arraylen){
			if(strcmp(array[a], "&") == 0){
				
				info.array = newarray;
				info.arraylen = start;
				info.error = merror;
				info.parallel = 0;
				//execute(newarray, start, merror, 0);
				pthread_t thread;
				if (pthread_create(&thread, NULL, executable, (void *)&info)) {
		            printerror();
		            return;
    			}
    			pthread_join(thread, NULL); 

				for (int i = 0; i < a; i++){
					newarray[i] = NULL;
				}

				start = 0;
				merror = -1;
			}
			else if(a + 1 == arraylen){
				char* p = array[a];
				newarray[start] = p;
				info.array = newarray;
				info.arraylen = start;
				info.error = merror;
				info.parallel = 0;
				//execute(newarray, start, merror, 0);
				pthread_t thread;
				if (pthread_create(&thread, NULL, executable, (void *)&info)) {
		            printerror();
		            return;
    			}
    			pthread_join(thread, NULL); 
				//execute(newarray, start+1, merror, 0);
			}
			else{
				char* p = array[a];
				newarray[start] = p;

		        if(strcmp(p, ">") == 0 && (merror == -1))
		    		merror = 0;
		    	else if (strcmp(p, ">") == 0 && (merror == 0)) //multiple redirection signs
		    		merror = 1;
		    	else if(strcmp(newarray[start-1], ">") != 0 && (merror == 0)){ //multiple files after redirection sign
		    		merror = 1;
		    	}

		    	start++;
			}
			a++;
		}
	}
	else if(strcmp(array[0], cd) == 0){ //if the user wants to change directory
		if(array[1] == NULL || array[2] != NULL){//no parameter or more than one parameter
    		printerror();
    	}
    	else{
    		chdir(array[1]);
    	}
	}

	else if(strcmp(array[0], ex) == 0){ // if the user wants to exit
		exit(0);
	}

	else if(strcmp(array[0], pa) == 0){ // if the path command is used
		if(array[1] == NULL){	// if there is nothing in the path argument
			for (int i = 0; i < 10; ++i){
				paths[i] = NULL;
			}
		}
		else{
			for (int i = 0; i < 10; ++i){ // remove all existing paths
				paths[i] = NULL;
			}
			int i = 1;
			while (i < 10 && array[i] != NULL){		//assuming there can be only 10 paths
				paths[i] = array[i];			// replace them with new paths the user specifies except index 0, which is reserved for the command the user enters
				i++;
			}
		}
	}

	else if(error == 1){ ////if the user tries to do redirection but there is an error
		printerror();
	}

	else if(error == 0){  //if the user tries to do redirection without an error
		//FILE *fp = fopen(array[arraylen-1], "w");
		//array[arraylen-2] = NULL;  //so that it will not try to add it to the execv. remember execv ends on a NULL value
		int i = 0;
		paths[0] = array[0]; //so that if the user specifies his own path, it will still run
		while(paths[i] != NULL && i < 10){
			char *path;
			if (i != 0){ //when i == 0, it means we are looking at the path the user specified and we do not want to add anthing to it
				path = malloc(strlen(paths[i]) + strlen(array[0]) + 1); 
				strcat(path, paths[i]);
				strcat(path, array[0]);
			}

			else{
				path = malloc(strlen(paths[0]) + 1); 
				strcat(path, paths[0]);
			}

			if (access(path, X_OK) == 0){ // check if the path exists
				pid_t pid = fork();

				if (pid < 0) {
		          	printerror();
		          	exit(1);
		     	}
			    else if (pid == 0) {
			    	int fd = open(array[arraylen-1], O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
			    	array[arraylen-2] = NULL;
			    	dup2(fd, 1);   // make stdout go to file
    				dup2(fd, 2);   // make stderr go to file
                   	close(fd);     
			        char *const parmList[] = {path, array[1], array[2], array[3], array[4], array[5], array[6], array[7], array[8], array[9], NULL};
			        execv(path, parmList);
			        printerror();
				}
				
				pid = wait(NULL);
				free(path);
				break;
			}

			i += 1;
		}
		if(paths[i] == NULL || i >= 10){ //if it looped through the available paths and it still did not run
			printerror();
		}
	}

	else{
		//printf("%s\n", array[arraylen-1]);
		int i = 0;
		paths[0] = array[0]; //so that if the user specifies his own path, it will still run
		while(paths[i] != NULL && i < 10){
			char *path;
			if (i != 0){ //when i == 0, it means we are looking at the path the user specified and we do not want to add anthing to it
				path = malloc(strlen(paths[i]) + strlen(array[0]) + 1); 
				strcat(path, paths[i]);
				strcat(path, array[0]);
			}

			else{
				path = malloc(strlen(paths[0]) + 1); 
				strcat(path, paths[0]);
			}

			if (access(path, X_OK) == 0){ // check if the path exists
				pid_t pid = fork();

				if (pid < 0) {
		          	printerror();
		          	exit(1);
		     	}
			    else if (pid == 0) {
			        char *const parmList[] = {path, array[1], array[2], array[3], array[4], array[5], array[6], array[7], array[8], array[9], NULL};
			        execv(path, parmList);
			        printerror();
				}
				
				pid = wait(NULL);
				free(path);
				break;
			}

			i += 1;
		}
		if(paths[i] == NULL || i >= 10){ //if it looped through the available paths and it still did not run
			printerror();
		}
	}
}

int main(int argc, char const *argv[]){
   	if(argc == 1) { //interactive mode
   		while(1){
   			int error = -1;
   			int parallel = 0;
	   		printf("wish> ");
	   		char *line = NULL;
	   		size_t length = 0;
			ssize_t nread;
			nread = getline(&line, &length, stdin);
			int len = strlen(line);
			if(line[len-1] == '\n') //remove terminating new line character
	    		line[len-1] = 0;

	    	char *buf = line;
		    int i = 0;
		    char *p = strtok (buf, " ");
		    char *array[10] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

		    while (p != NULL){
		        array[i] = p;
		        if(strcmp(p, ">") == 0 && (error == -1))
		    		error = 0;
		    	else if (strcmp(p, ">") == 0 && (error == 0)) //multiple redirection signs
		    		error = 1;
		    	else if(strcmp(array[i-1], ">") != 0 && (error == 0)){ //multiple files after redirection sign
		    		error = 1;
		    	}
		    	if(strcmp(p, "&") == 0 )
		    		parallel = 1;
		        i += 1;
		        p = strtok (NULL, " ");
		    }

		    execute(array, i, error, parallel);
		}
   	}

   	else if(argc == 2){ //batch mode
   		FILE *fp = fopen(argv[1], "r");
		if (fp == NULL){
			printerror();
			exit(1);
		}
		char *line = NULL;
		size_t length = 0;
		ssize_t nread;
		nread = getline(&line, &length, fp);

		while(nread != -1){
			int error = -1;
			int parallel = 0;
			int len = strlen(line);
			if(line[len-1] == '\n') //remove terminating new line character
	    		line[len-1] = 0;

	    	char *buf = line;
		    int i = 0;
		    char *p = strtok (buf, " ");
		    char *array[10] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

		    while (p != NULL){
		        array[i] = p;
		        if(strcmp(p, ">") == 0 && (error == -1)){
		    		error = 0;
		        }
		    	else if (strcmp(p, ">") == 0 && (error == 0)) //multiple redirection signs
		    		error = 1;
		    	else if(i != 0 && strcmp(array[i-1], ">") != 0 && (error == 0)){ //multiple files after redirection sign
		    		error = 1;
		    	}
		    	if(strcmp(p, "&") == 0 )
		    		parallel = 1;

		        i += 1;
		        p = strtok (NULL, " ");
		    }
		    execute(array, i, error, parallel);
			nread = getline(&line, &length, fp);
		}
		fclose(fp);
   	}

}
