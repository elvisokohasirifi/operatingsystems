def traingular(n): #this funtion computes the nth triangular number
	if(n == 1):
		return 1
	else:
		return traingular(n-1) + n

def main(n): #this function finds the sum of the first n triangular numbers
	if(n <= 0):
		print(0)
		return
	summ = 0
	for i in range(1, n+1):
		summ += traingular(i)
	return summ
	
print(main(4))

