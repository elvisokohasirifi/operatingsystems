#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

char *paths[10] = {NULL, "/bin/", "/usr/bin/"}; // the null is for the command the user enters, assuming that the user may enter the entire path of the command. For example, /bin/ls. It should still work regardless

// this function takes the users command that has been splitted by spaces into an array and the various paths the program can access
void execute(char **array, char **paths){
	char *ex = "exit";
	char *pa = "path";
	char *cd = "cd";
	if(strcmp(array[0], cd) == 0){ //if the user wants to change directory
		if(array[1] == NULL || array[2] != NULL){//no parameter or more than one parameter
    		printf("Directory cannot be changed. Provide only one argument\n");
    	}
    	else{
    		chdir(array[1]);
    	}
	}

	else if(strcmp(array[0], ex) == 0){ // if the user wants to exit
		exit(0);
	}

	else if(strcmp(array[0], pa) == 0){ // if the path command is used
		if(array[1] == NULL){	// if there is nothing in the path argument
			for (int i = 0; i < 10; ++i){
				paths[i] = NULL;
			}
		}
		else{
			for (int i = 0; i < 10; ++i){ // remove all existing paths
				paths[i] = NULL;
			}
			int i = 1;
			while (i < 10 && array[i] != NULL){		//assuming there can be only 10 paths
				paths[i] = array[i];			// replace them with new paths the user specifies except index 0, which is reserved for the command the user enters
				i++;
			}
		}
	}

	else{
		int i = 0;
		paths[0] = array[0]; //so that if the user specifies his own path, it will still run
		while(paths[i] != NULL && i < 10){
			char *path;
			if (i != 0){ //when i == 0, it means we are looking at the path the user specified and we do not want to add anthing to it
				path = malloc(strlen(paths[i]) + strlen(array[0]) + 1); 
				strcat(path, paths[i]);
				strcat(path, array[0]);
			}

			else{
				path = malloc(strlen(paths[0]) + 1); 
				strcat(path, paths[0]);
			}

			if (access(path, X_OK) == 0){ // check if the path exists
				pid_t pid = fork();

				if (pid < 0) {
		          	printf("Failed to fork process 1\n");
		          	exit(1);
		     	}
			    else if (pid == 0) {
			        char *const parmList[] = {path, array[1], array[2], array[3], array[4], array[5], array[6], array[7], array[8], array[9], NULL};
			        execv(path, parmList);
			        printf("Sorry, that didn't work 1\n");
				}
				
				pid = wait(NULL);
				free(path);
				break;
			}

			i += 1;
		}
		if(paths[i] == NULL || i >= 10){ //if it looped through the available paths and it still did not run
			printf("The command/path does not exist\n");
		}
	}
}

int main(int argc, char const *argv[]){
   	if(argc == 1) { //interactive mode
   		while(1){
	   		printf("wish> ");
	   		char *line = NULL;
	   		size_t length = 0;
			ssize_t nread;
			nread = getline(&line, &length, stdin);
			int len = strlen(line);
			if(line[len-1] == '\n') //remove terminating new line character
	    		line[len-1] = 0;

	    	char *buf = line;
		    int i = 0;
		    char *p = strtok (buf, " ");
		    char *array[10] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

		    while (p != NULL){
		        array[i] = p;
		        i += 1;
		        p = strtok (NULL, " ");
		    }
		    execute(array, paths);
		}
   	}

   	else if(argc == 2){ //batch mode
   		FILE *fp = fopen(argv[1], "r");
		if (fp == NULL){
			printf("cannot open file\n");
			exit(1);
		}
		char *line = NULL;
		size_t length = 0;
		ssize_t nread;
		nread = getline(&line, &length, fp);

		while(nread != -1){
			int len = strlen(line);
			if(line[len-1] == '\n') //remove terminating new line character
	    		line[len-1] = 0;

	    	char *buf = line;
		    int i = 0;
		    char *p = strtok (buf, " ");
		    char *array[10] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

		    while (p != NULL){
		        array[i] = p;
		        i += 1;
		        p = strtok (NULL, " ");
		    }
		    execute(array, paths);
			nread = getline(&line, &length, fp);
		}
   	}

}
