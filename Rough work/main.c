#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include<pthread.h>

//basic node structure
typedef struct __node_t{
	int key;
	struct __node_t * next;
} node_t;

//basic list structure
typedef struct __list_t{
	node_t * head;
	pthread_mutex_t lock;
	int size;
} list_t;

//Init 
void List_Init(list_t *L){
	L->head = NULL;
	pthread_mutex_init(&L->lock, NULL);
	L->size = 0;
}

//insert
void List_Insert(list_t *L, int key){
	node_t * new = malloc(sizeof(node_t));
	if(new == NULL){
		perror("malloc");
		return;
	}

	new ->key = key;
	pthread_mutex_lock(&L->lock);
	new->next = L->head;
	L->head = new;
	L->size += 1;
	pthread_mutex_unlock(&L->lock);
}

int List_Delete(list_t *L, int key){
	int c =-1;
	pthread_mutex_lock(&L->lock);
	node_t* cur = L->head, *prev; 
	if(cur !=NULL && cur->key == key){
	 	L->head = cur->next;
	 	free(cur);
	 	c =0;
	 	L->size -= 1;
	}
	else{
	 	while(cur != NULL && cur->key!=key){
	 		prev = cur;
	 		cur = cur->next;
	 	}

	 	if(cur == NULL) {
	 		c = -1;
	 	}
	 	else{
	 		prev->next = cur->next;
			c = 0;
			L->size -= 1;
			free(cur);
		}
	}
	pthread_mutex_unlock(&L->lock);
	return c;
}

int List_Lookup(list_t * L, int key){
	int rv = -1;
	pthread_mutex_lock(&L->lock);
	node_t * curr = L->head;
	while(curr){
		if(curr->key == key){
			rv = 0;
			break;
		}
		curr = curr->next;
	}
	pthread_mutex_unlock(&L->lock);
	return rv;
}

int List_Size(list_t *L){
	return L->size;
}

int main(){
	list_t list_t;
	//initialise the list
	List_Init(&list_t);

	//insert into the list: key 1, 2, and 3
	List_Insert(&list_t, 1);
	List_Insert(&list_t, 2);
	List_Insert(&list_t, 3);
	printf("%d\n", List_Size(&list_t));

	/*//delete key 5: returns 0 if successfully deleted or -1 if key is not found
	printf("%d\n", List_Delete(&list_t, 8));

	//delete key 3: returns 0 if successfully deleted or -1 if key is not found
	printf("%d\n", List_Delete(&list_t, 3));

	//lookup key 1: return 0 is key exists or -1 if key does not exist
	printf("%d\n", List_Lookup(&list_t, 1));

	//lookup key 2: return 0 is key exists or -1 if key does not exist
	printf("%d\n", List_Lookup(&list_t, 2));

	//lookup key 8: return 0 is key exists or -1 if key does not exist
	printf("%d\n", List_Lookup(&list_t, 8));

	//lookup key 3: return 0 is key exists or -1 if key does not exist
	printf("%d\n", List_Lookup(&list_t, 3));
	*/
}