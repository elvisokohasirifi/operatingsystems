#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>


typedef struct __counter_t {
	int value;
} counter_t;

void init(counter_t *c) {
	c->value = 0;
}

void increment(counter_t *c) {
	c->value++;
}

void decrement(counter_t *c) {
	c->value--;
}

int get(counter_t *c) {
	return c->value;
}


typedef struct __concurrent_counter_t {
	int value;
	pthread_mutex_t lock;
} concurrent_counter_t;

void initc(concurrent_counter_t *c) {
	c->value = 0;
	Pthread_mutex_init(&c->lock, NULL);
}

void incrementc(concurrent_counter_t *c) {
	Pthread_mutex_lock(&c->lock);
	c->value++;
	Pthread_mutex_unlock(&c->lock);
}

void decrementc(concurrent_counter_t *c) {
	Pthread_mutex_lock(&c->lock);
	c->value--;
	Pthread_mutex_unlock(&c->lock);
}

int getc(concurrent_counter_t *c) {
	Pthread_mutex_lock(&c->lock);
	int rc = c->value;
	Pthread_mutex_unlock(&c->lock);
	return rc;
}
