#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char const *argv[])
{
	if( argc == 1) {
		printf("wish> ");
		char *line = NULL;
		char *ex = "exit";
		char *pa = "path";
		char *cd = "cd";
		size_t len = 0;
		ssize_t nread;
		char *path1 = "/bin/";
		char *path2 = "/usr/bin/";
		nread = getline(&line, &len, stdin);
		char *path;
		char *another;
		char *paths[10] = {"/bin/", "/usr/bin/", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};
		len = strlen(line);
		if(line[len-1] == '\n') //remove terminating new line character
    		line[len-1] = 0;
		while (strcmp(line, ex) != 0){ //while the user has not typed exit
			pid_t pid;

			char *buf = line;
		    int i = 0;
		    char *p = strtok (buf, " ");
		    char *array[10] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

		    while (p != NULL){
		        array[i] = p;
		        i += 1;
		        p = strtok (NULL, " ");
		    }

		    if (strcmp(array[0], cd) == 0){
		    	if(array[1] == NULL || array[2] != NULL){//no parameter or more than one parameter
		    		printf("Directory cannot be changed. Provide only one argument\n");
		    	}
		    	else{
		    		chdir(array[1]);
		    	}
		    }

		    else if(strcmp(array[0], pa) == 0){

		    }

		    else{
			    path = malloc(strlen(path2) + strlen(array[0]) + 1); 
				strcat(path, path2);
				strcat(path, array[0]);
				another= malloc(strlen(path1) + strlen(array[0]) + 1); 
				strcat(another, path1);
				strcat(another, array[0]);

				if (access(path, X_OK) == 0){
					pid = fork();

					if (pid < 0) {
			          	printf("Failed to fork process 1\n");
			          	exit(1);
			     	}
				    else if (pid == 0) {
				        char *const parmList[] = {path, array[1], array[2], array[3], array[4], array[5], array[6], array[7], array[8], array[9], NULL};
				        execv(path, parmList);
				        printf("Sorry, that didn't work 1\n");
					}
					
					pid = wait(NULL);
					
				}
				else if(access(another, X_OK) == 0){
					pid = fork();

					if (pid < 0) {
			          	printf("Failed to fork process 1\n");
			          	exit(1);
			     	}
				    else if (pid == 0) {
				        char *const parmList[] = {another, array[1], array[2], array[3], array[4], 
				        	array[5], array[6], array[7], array[8], array[9], NULL};
				        execv(another, parmList);
				        printf("Sorry, that didn't work 2\n");
					}
					 
					pid = wait(NULL);
				}
				else{
					printf("Sorry, the operation cannot be performed\n");
					exit(1);
					return 0;
				}
				free(path);
				free(another);
			}
			printf("wish> ");
			nread = getline(&line, &len, stdin);
			len = strlen(line);
			if( line[len-1] == '\n')
    			line[len-1] = 0;
			
		}
		exit(0);
   	}


   	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   	else if(argc == 2) {
      	
		FILE *fp = fopen(argv[1], "r");
		//char *line = NULL;
		char *line = NULL;
		char *ex = "exit";
		size_t len = 0;
		ssize_t nread;
		char *path1 = "/usr/bin/";
		char *path2 = "/bin/";
		nread = getline(&line, &len, fp);
		int as = 0;
		char *path;
		char *another;
		char *pa = "path";
		char *cd = "cd";
		len = strlen(line);
		if(line[len-1] == '\n') //remove terminating new line character
    		line[len-1] = 0;

		while (strcmp(line, ex) != 0 && nread != -1) {
			pid_t pid;
			char *buf = line;
		    int i = 0;
		    char *p = strtok (buf, " ");
		    char *array[10] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

		    while (p != NULL){
		        array[i] = p;
		        i += 1;
		        p = strtok (NULL, " ");
		    }

		    if (strcmp(array[0], cd) == 0){
		    	if(array[1] == NULL || array[2] != NULL){//no parameter or more than one parameter
		    		printf("Directory cannot be changed. Provide only one argument\n");
		    	}
		    	else{
		    		chdir(array[1]);
		    	}
		    }

		    else if(strcmp(array[0], pa) == 0){

		    }

		    path = malloc(strlen(path2) + strlen(array[0]) + 1); 
			strcat(path, path2);
			strcat(path, array[0]);
			another= malloc(strlen(path1) + strlen(array[0]) + 1); 
			strcat(another, path1);
			strcat(another, array[0]);

			if (access(path, F_OK) == 0){
				pid = fork();

				if (pid < 0) {
		          	printf("Failed to fork process 1\n");
		          	exit(1);
		     	}
			    else if (pid == 0) {
			        char *const parmList[] = {path, array[1], array[2], array[3], array[4], array[5], array[6], array[7], array[8], array[9], NULL};
			        execv(path, parmList);
			        printf("Sorry, that didn't work 1\n");
				}
				
				pid = wait(NULL);
				
			}
			else if(access(another, F_OK) == 0){
				pid = fork();

				if (pid < 0) {
		          	printf("Failed to fork process 1\n");
		          	exit(1);
		     	}
			    else if (pid == 0) {
			        char *const parmList[] = {another, array[1], array[2], array[3], array[4], 
			        	array[5], array[6], array[7], array[8], array[9], NULL};
			        execv(another, parmList);
			        printf("Sorry, that didn't work 2\n");
				}
				 
				pid = wait(NULL);
			}
			else{
				printf("Sorry, the operation cannot be performed\n");
				exit(1);
				return 0;
			}
			
			nread = getline(&line, &len, fp);

			len = strlen(line);
			if( line[len-1] == '\n')
    			line[len-1] = 0;
			//free(path);
			//free(another);
		}
		free(path);
		free(another);
		exit(0);
	}
	return 0;
}