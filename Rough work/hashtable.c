#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

#define BUCKETS (101)

typedef struct __hash_t {
	list_t lists[BUCKETS];
} hash_t;

void Hash_Init(hash_t *H) {
	int i;
	for (i = 0; i < BUCKETS; i++) {
		List_Init(&H->lists[i]);
	}
}

int Hash_Insert(hash_t *H, int key) {
	int bucket = key % BUCKETS;
	return List_Insert(&H->lists[bucket], key);
}

int Hash_Lookup(hash_t *H, int key) {
	int bucket = key % BUCKETS;
	return List_Lookup(&H->lists[bucket], key);
}

int main(int argc, char const *argv[])
{
	hash_t hashtable;
	Hash_Init(hashtable);
	Hash_Insert(hashtable, 7);
	Hash_Insert(hashtable, 8);
	Hash_Insert(hashtable, 9);
}
