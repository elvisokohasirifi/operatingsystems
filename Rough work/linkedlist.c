#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

// basic node structure
typedef struct __concurrent_node_t {
	int key;
	struct __concurrent_node_t *next;
} concurrent_node_t;
// basic list structure (one used per list)
typedef struct __concurrent_list_t {
	concurrent_node_t *head;
	pthread_mutex_t lock;
} concurrent_list_t;

void List_Initc(concurrent_list_t *L) {
	printf("here 3\n");
	L->head = NULL;
	printf("here 4\n");
	pthread_mutex_init(&L->lock, NULL);
}

int List_Insertc(concurrent_list_t *L, int key) {
	printf("here 2\n");
	pthread_mutex_lock(&L->lock);
	concurrent_node_t *new = malloc(sizeof(concurrent_node_t));
	if (new == NULL) {
		perror("malloc");
		pthread_mutex_unlock(&L->lock);
	 return -1; // fail
	}

	new->key = key;
	new->next = L->head;
	L->head = new;

	pthread_mutex_unlock(&L->lock);
	 return 0; // success
}

int List_Lookupc(concurrent_list_t *L, int key) {
	pthread_mutex_lock(&L->lock);
	concurrent_node_t *curr = L->head;
	while (curr) {
		if (curr->key == key) {
			pthread_mutex_unlock(&L->lock);
	 	return 0; // success
	}
	curr = curr->next;
	}
	pthread_mutex_unlock(&L->lock);
	return -1; // failure
}

void *insertall(void *args){
	concurrent_list_t *lists = args;	
	for (int i = 0; i < 100; ++i)
	{
		List_Insertc(lists, i * 2);
	}
	printf("Child done\n");
	pthread_exit(NULL);
    return NULL;
}

int main(int argc, char const *argv[])
{
	concurrent_list_t *list; 
	printf("Here\n");
	List_Initc(list);
	//pthread_t *thread;
	//pthread_create(thread, NULL, insertall, (void *)&list);
	for (int i = 0; i < 10; ++i)
	{
		List_Insertc(list, i * 2);
	}
	printf("Parent Done\n");
	return 0;
}
