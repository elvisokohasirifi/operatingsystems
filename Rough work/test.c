#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>


int main(int argc, char const *argv[]){
	FILE *fp = fopen(argv[1], "r");
		//char *line = NULL;
		size_t len = 0;
		ssize_t nread;
		char *ex = "exit";
		char *path1 = "/usr/bin/";
		char *path2 = "/bin/";
		char line[1000];

		if (fp == NULL) {
		       printf("Cannot open file\n");
		       exit(1);
		}

		while (fgets(line, 1000, fp) != NULL) {
			if(line[len-1] == '\n') //remove terminating new line character
    			line[len-1] = 0;
			int i = 0;
			char *buf = line;
		    char *p = strtok (buf, " ");
		    char *array[10] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

		    while (p != NULL){
		        array[i] = p;
		        i += 1;
		        p = strtok (NULL, " ");
		    }
			char *path = malloc(strlen(path2) + strlen(array[0]) + 1); 
			strcat(path, path2);
			strcat(path, array[0]);
			printf("%s", path);
			//free(path);
		}

		fclose(fp);


    return 0;
}