#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>


// basic node structure
typedef struct node_t {
	int key;
	struct node_t *next;
} node_t;


//Locked Functions (Mutually exclusive)

// basic list structure (one used per list)
typedef struct __locked_list_t {
	node_t *head;
	int n_nodes;
} locked_list_t;

void List_Init_m(locked_list_t *L) {
	L->head = NULL;
}

int List_Insert_m(locked_list_t *L, int key) {
	node_t *new = malloc(sizeof(node_t));
	if (new == NULL) {
		perror("malloc");
	 return -1; // fail
	}

	new->key = key;
	new->next = L->head;
	L->head = new;
	L->n_nodes++;
	return 0; // success
}

int List_Lookup_m(locked_list_t *L, int key) {
	node_t *curr = L->head;
	while (curr) {
		if (curr->key == key) {
	 	return 0; // success
	}
	curr = curr->next;
	}
	return -1; // failure
}

int List_Size(locked_list_t *L){
	return L->n_nodes;
}


void delete_m(locked_list_t *list_m, int key){
    //beginning from the head of the list,
    //loop through the list and check if the key is the same as the user-provided key
    //if it is, set the found flag to 1, break the loop,
    //set the current node to its next, then the next node to the next of that one,
    //effectively deleting the current node

	node_t *current, *previous;
	current = list_m->head, *previous;
	int found = 0;
	while (found == 0 && current != NULL)
	{
		if (current->key == key)
		{
			found = 1;
		}
		else
		{
			previous = current;
			current = current->next;
		}
	}
	if (found == 1)
	{
		previous->next = current->next;
		//current->next = current->next->next;
		list_m->n_nodes--;
	}
	free(current);
	//free(previous);
}


locked_list_t hashtable;

//delete from 0 to 1000000
void *executable(void *args){
	for (int i = 0; i < 10000; ++i)
	{
		delete_m(&hashtable, i);
	}
	pthread_exit(NULL);
    return NULL;
}

int main(int argc, char const *argv[])
{
	List_Init_m(&hashtable);
	for (int i = 0; i < 20000; ++i)
	{
		List_Insert_m(&hashtable, i);
	}
	printf("Size after insertions = %d\n", List_Size(&hashtable));
	pthread_t thread;
	if (pthread_create(&thread, NULL, executable, NULL)) {
        printf("An error occured\n");
        return 0;
	}
	//delete from 10000 to 20000
	for (int i = 10000; i < 20000; ++i)
	{
		delete_m(&hashtable, i);
	}
	pthread_join(thread, NULL);
	printf("Size after concurrent deletions = %d\n", List_Size(&hashtable));
}
