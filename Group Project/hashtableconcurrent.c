#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include<pthread.h>

//node structure. the hashtable will be implemented using a linked list
typedef struct __node_t{
	int key;
	struct __node_t * next;
} node_t;

/*
	struct for a linkedlist. contains a head, 
	number of items in the list so far and a lock
*/
typedef struct __list_t{
	node_t * head;
	pthread_mutex_t lock;
	int size;
} list_t;

/*
	function that initialise the linkedlist. it sets the head
	to NULL, initialises the lock and sets size to 0
*/
void List_Init(list_t *L){
	L->head = NULL;
	pthread_mutex_init(&L->lock, NULL);
	L->size = 0;
}

/*
	inserts a new entry into the list. throws an error 
	when the memory allocation fails
*/
void List_Insert(list_t *L, int key){
	node_t * new = malloc(sizeof(node_t));
	if(new == NULL){
		perror("allocation error");
		return;
	}

	new ->key = key;
	pthread_mutex_lock(&L->lock);
	new->next = L->head;
	L->head = new;
	L->size += 1;
	pthread_mutex_unlock(&L->lock);
}

int List_Delete(list_t *L, int key){
	int c =-1;
	pthread_mutex_lock(&L->lock);
	node_t* cur = L->head, *prev; 

	//if the list is not empty and the item to be 
	// removed is the head
	if(cur !=NULL && cur->key == key){
	 	L->head = cur->next;
	 	free(cur);
	 	c =0;
	 	L->size -= 1;
	}
	else{
	 	while(cur != NULL && cur->key != key){
	 		prev = cur;
	 		cur = cur->next;
	 	}

	 	if(cur == NULL) {
	 		c = -1;
	 	}
	 	else{
	 		prev->next = cur->next;
			c = 0;
			L->size -= 1;
			free(cur);
		}
	}
	pthread_mutex_unlock(&L->lock);
	return c;
}

/*
	find the index of an element in the list. 
	returns -1 if it is not in the list
*/
int List_Lookup(list_t * L, int key){
	int rv = -1;
	pthread_mutex_lock(&L->lock);
	node_t * curr = L->head;
	while(curr){
		if(curr->key == key){
			rv = 0;
			break;
		}
		curr = curr->next;
	}
	pthread_mutex_unlock(&L->lock);
	return rv;
}

/*
	returns the number of items in the list
*/
int List_Size(list_t *L){
	return L->size;
}

/*
	hashtable structure. It is implement as an array of linkedlists. 
	the linkedlists act as buckets.
*/
typedef struct __hash_t{
	list_t array [101];
	pthread_mutex_t lock;
} hash_t;

/*
	initialise the hashtable. initialise the linked lists in the array. 
	initialise the lock
*/
void Hash_Init(hash_t *L){
	for (int i = 0; i < 101; i++)
	{
		List_Init(&L->array[i]);
	}
	pthread_mutex_init(&L->lock, NULL);
}

/*
	compute the hash value, which determines the index of the
	linkedlist that will store the key within the array. 
	calls on the linkedlist insert method to do the insertion. 
	the method is already atomic
*/
void Hash_Insert(hash_t *H, int key) {
	int bucket = key % 101;
	return List_Insert(&H->array[bucket], key);
}

/*
	finds the position of key within the linkedlist that holds it.
	calls the linkedlist lookup function
*/
int Hash_Lookup(hash_t *H, int key) {
	int bucket = key % 101;
	return List_Lookup(&H->array[bucket], key);
}

/*
	delete an item from the hashtable
*/
int Hash_Delete(hash_t *H, int key){
	int bucket = key % 101;
	return List_Delete(&H->array[bucket], key);
}

/*
	returns the number of items in the hashtable
*/
int Hash_Size(hash_t *H){
	int n = 0;
	for (int i = 0; i < 101; ++i)
	{
		n += List_Size(&H->array[i]);
	}
	return n;
}

hash_t hashtable;

void *executable(void *args){
	for (int i = 100000; i < 200000; ++i)
	{
		Hash_Delete(&hashtable, i);
	}
	pthread_exit(NULL);
    return NULL;
}

int main(int argc, char const *argv[])
{
	Hash_Init(&hashtable);

	//insert 200000 items in the hashtable non-concurrently
	for (int i = 0; i < 200000; ++i)
	{
		Hash_Insert(&hashtable, i);
	}
	printf("Size after insertions: %d\n", Hash_Size(&hashtable));
	//use 2 threads to remove all the contents of the hashtable
	pthread_t thread;
	if (pthread_create(&thread, NULL, executable, NULL)) {
        printf("An error occured\n");
        return 0;
	}
	for (int i = 0; i < 100000; ++i)
	{
		Hash_Delete(&hashtable, i);
	}
	pthread_join(thread, NULL);
	printf("Size after concurrent deletions: %d\n", Hash_Size(&hashtable));
}

