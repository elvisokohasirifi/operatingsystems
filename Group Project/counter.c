
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

/**
	the counter struct with value as the current 
	count of the counter

	this counter does not have locks so it is not concurrent
**/
typedef struct __counter_t {
  int value;
} counter_t;

counter_t counter; // a static counter that will be accessed by the parent and child threads

/*
	initialising the counter. The value is set to 0
*/
void init(counter_t *c) {
  c->value = 0;
}

/*
	a function that increments the counter by a certain value
*/
void increment_by(counter_t *c, int by) {
  c->value += by;
}

/*
	calls the increment_by function and passes 1 as 
	the int by
*/
void increment(counter_t *c) {
  increment_by(c, 1);
}

/*
	returns the current value of the counter
*/
int get(counter_t *c) {
  int rc = c->value;
  return rc;
}

/*
	reduces the counter value by 1. calls on the increment_by 
	function and passes -1 to it to reduce the current value by 1
*/
void decrement(counter_t *c){
  increment_by(c, -1);
}

/*
	reduces the counter value by a margin. It calls the increment_by function 
	but negates the value passed to it to make it reduce rather than add
*/
void decrement_by(counter_t *c, int val){
  increment_by(c, val * -1);
}

/*
	the function that will be called by the thread. 
	It increments the counter value by 1000000
*/
void *executable(void *args){
  for (int i = 0; i < 1000000; ++i)
  {
    decrement(&counter);
  }
  pthread_exit(NULL);
    return NULL;
}

/*
	main function. Creates a child thread to execute the 
	function above. The parent thread itself also 
	increments the counter value by 1000000. Expected result is < 2000000
*/
int main(int argc, char const *argv[])
{
  init(&counter);
  //increment counter by 2000000 times. No threads used
  for (int i = 0; i < 2000000; ++i)
  {
    increment(&counter);
  }
  printf("Value of counter after increment: %d\n", get(&counter));

  // now were are going to use 2 threads to delete the counter by 2000000
  pthread_t thread;
  if (pthread_create(&thread, NULL, executable, NULL)) {
        printf("An error occured\n");
        return 0;
  }
  for (int i = 0; i < 1000000; ++i)
  {
    decrement(&counter);
  }
  //pthread_join(thread, NULL);
  printf("Value of counter after concurrent decrement: %d\n", get(&counter));
}
