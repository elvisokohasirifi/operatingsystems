#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <assert.h>

typedef struct __node_t {
	int value;
	struct __node_t *next;
} node_t;

typedef struct __queue_t {
	node_t *head;
	node_t *tail;

	int size;
} queue_t;


void Queue_Init(queue_t *q) {
	node_t *tmp = malloc(sizeof(node_t));
	tmp->next = NULL;
	q->head = q->tail = tmp;
	q->size = 0;
}

void Queue_Enqueue(queue_t *q, int value) {
	node_t *tmp = malloc(sizeof(node_t));
	assert(tmp != NULL);
	tmp->value = value;
	tmp->next = NULL;
	q->tail->next = tmp;
	q->tail = tmp;
	q->size++;
}

int Queue_Dequeue(queue_t *q) {
	node_t *tmp = q->head;
	node_t *newHead = tmp->next;
	if (newHead == NULL) {
	 	return -1; // queue was empty
	}

	int value = newHead->value;
	q->head = newHead;
	q->size--;
	free(tmp);
	return value;
}

int Queue_Size(queue_t *q){
	return q->size;
}


queue_t hashtable;

void *executable(void *args){
	for (int i = 0; i < 1000000; ++i)
	{
		Queue_Dequeue(&hashtable);
	}
	pthread_exit(NULL);
    return NULL;
}

int main(int argc, char const *argv[])
{
	Queue_Init(&hashtable);
	//insert 2000000 items non-concurrently
	for (int i = 0; i < 2000000; ++i)
	{
		Queue_Enqueue(&hashtable, i);
	}
	printf("Size after insertions = %d\n", Queue_Size(&hashtable));
	//use 2 threads to remove all items inserted
	//the threads should throw an error because
	//at some point the two threads will try to
	//free the tmp variable in the dequeue function
	//at the same time. The error is a 
	//double free or corruption (fasttop) error
	pthread_t thread;
	if (pthread_create(&thread, NULL, executable, NULL)) {
        printf("An error occured\n");
        return 0;
	}
	for (int i = 0; i < 1000000; ++i)
	{
		Queue_Dequeue(&hashtable);
	}
	pthread_join(thread, NULL);
	printf("Size after concurrent deletions = %d\n", Queue_Size(&hashtable));
}
