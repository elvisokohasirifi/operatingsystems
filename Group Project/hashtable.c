#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <pthread.h> // for threads
#include <sys/stat.h>

//basic node structure
typedef struct __node_t{
	int key;
	struct __node_t * next;
} node_t;

//basic list structure
typedef struct __list_t{
	node_t * head;
	int size;
} list_t;

//Init 
void List_Init(list_t *L){
	L->head = NULL;
	L->size = 0;
}

//insert
void List_Insert(list_t *L, int key){
	node_t * new = malloc(sizeof(node_t));
	if(new == NULL){
		perror("malloc");
		return;
	}

	new ->key = key;
	new->next = L->head;
	L->head = new;
	L->size += 1;
}

int List_Delete(list_t *L, int key){
	int c =-1;
	node_t* cur = L->head, *prev; 
	if(cur !=NULL && cur->key == key){
	 	L->head = cur->next;
	 	free(cur);
	 	c =0;
	 	L->size -= 1;
	}
	else{
	 	while(cur != NULL && cur->key!=key){
	 		prev = cur;
	 		cur = cur->next;
	 	}

	 	if(cur == NULL) {
	 		c = -1;
	 	}
	 	else{
	 		prev->next = cur->next;
			c = 0;
			L->size -= 1;
			free(cur);
		}
	}
	return c;
}

int List_Lookup(list_t * L, int key){
	int rv = -1;
	node_t * curr = L->head;
	while(curr){
		if(curr->key == key){
			rv = 0;
			break;
		}
		curr = curr->next;
	}
	return rv;
}

int List_Size(list_t *L){
	return L->size;
}

typedef struct __hash_t{
	list_t array [101];
	pthread_mutex_t lock;
} hash_t;

void Hash_Init(hash_t *L){
	for (int i = 0; i < 101; i++)
	{
		List_Init(&L->array[i]);
	}
}

void Hash_Insert(hash_t *H, int key) {
	int bucket = key % 101;
	return List_Insert(&H->array[bucket], key);
}

int Hash_Lookup(hash_t *H, int key) {
	int bucket = key % 101;
	return List_Lookup(&H->array[bucket], key);
}

int Hash_Delete(hash_t *H, int key){
	int bucket = key % 101;
	return List_Delete(&H->array[bucket], key);
}

int Hash_Size(hash_t *H){
	int n = 0;
	for (int i = 0; i < 101; ++i)
	{
		n += List_Size(&H->array[i]);
	}
	return n;
}

hash_t hashtable;

void *executable(void *args){
	for (int i = 100000; i < 200000; ++i)
	{
		Hash_Delete(&hashtable, i);
	}
	pthread_exit(NULL);
    return NULL;
}

int main(int argc, char const *argv[])
{
	Hash_Init(&hashtable);

	//insert 200000 items in the hashtable non-concurrently
	for (int i = 0; i < 200000; ++i)
	{
		Hash_Insert(&hashtable, i);
	}
	printf("Size after insertions: %d\n", Hash_Size(&hashtable));
	//use 2 threads to remove all the contents of the hashtable
	pthread_t thread;
	if (pthread_create(&thread, NULL, executable, NULL)) {
        printf("An error occured\n");
        return 0;
	}
	for (int i = 0; i < 100000; ++i)
	{
		Hash_Delete(&hashtable, i);
	}
	pthread_join(thread, NULL);
	//the size remaining is very random.
	printf("Size after concurrent deletions: %d\n", Hash_Size(&hashtable));
}