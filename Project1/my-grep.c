#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

int main(int argc, char *argv[]){
	if( argc == 1) {
		printf("my-grep: searchterm [file ...]\n");
		exit(1);
   	}
	else if( argc == 2) {
		char str[100000];
		fgets(str, 100000, stdin);
		puts(str);
		
   	}

   	else {
      		for(int i = 2; i < argc; i++){
			FILE *stream = fopen(argv[i], "r");
			char *line = NULL;
			size_t len = 0;
			ssize_t nread;

			if (stream == NULL) {
			       printf("my-grep: cannot open file\n");
			       exit(1);
			}

			while ((nread = getline(&line, &len, stream)) != -1) {
				char *found = strstr(line, argv[1]);
				if(found != NULL)
					printf("%s", line);
			}

			free(line);
			fclose(stream);
		}
	}
	return 0;
}
