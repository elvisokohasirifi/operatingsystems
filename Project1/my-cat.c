#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define SIZE 10

int main(int argc, char *argv[]){
	if( argc == 1) {
		exit(1);
   	}
   	else {
      		for(int i = 1; i < argc; i++){
			FILE *fp = fopen(argv[i], "r");
			char buffer[SIZE];
			if (fp == NULL){
				printf("my-cat: cannot open file\n");
				exit(1);
			}
			
			else{
				while(fgets(buffer, SIZE, fp) != NULL){
					printf("%s", buffer);
				}
			}	
			fclose(fp);
		}
	}
	return 0;
}
