In ubuntu you need to install build-essential for C and C++ Compilers for development work if you are a developer.

Just run following command in terminal and all the required packages for C and C++ compilers will be installed.

sudo aptitude install build-essential
Now you can test your first C and C++ programs.

1.Type following to open mytest.c file.

sudo gedit mytest.c
Add these lines and save the file.

#include <stdio.h>

int main()

{

printf(“Hello,World!\n”);

}

Compile the code by the following command

cc -c mytest.c
Type following to create an executable

cc -o mytest mytest.c
Now you can run it by this command

./mytest
2.To run a C++ program ,first create mytest.cpp by type:

sudo gedit mytest.cpp
add this code to the file and save.

#include <iostream>

int main()

{

std::cout<<”Hello,World!”<<std::endl;

}

Then run this program by following command:

g++ mytest.cpp -o mytest

./mytest
